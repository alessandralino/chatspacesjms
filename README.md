#Trabalho de Espaço de Tuplas e MOM: Chat com JavaSpaces e JMS

Objetivo: 
Desenvolver um Chat 

#Execução

1. Importar projeto na IDE Eclipse Mars ou realizar git clone;
2. Adcionar os .jar da pasta Apache River ao Build Path do projeto;
3. Startar o Apache River (sh start-services.sh);
4. Executar a classe Registro.java pacote view, uma vez para cada usuário.


#Dados do desenvolvedor

Nome: Alessandra Lino Cavalcante 

#IDE

Eclipse Java EE IDE for Web Developers.
Version: Mars.2 Release (4.5.2) Build id: 20160218-0600

#Compilador/Interpretador

java version "1.7.0_101"
OpenJDK Runtime Environment (IcedTea 2.6.6) (7u101-2.6.6-0ubuntu0.14.04.1)
OpenJDK 64-Bit Server VM (build 24.95-b01, mixed mode)


#Sistema Operacional

elementary OS 0.3 Freya (64-bit) - Construído sobre Ubuntu 14.04

#APIS (dist/lib) 
-