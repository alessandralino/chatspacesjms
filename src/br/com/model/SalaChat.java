package br.com.model;

import net.jini.core.entry.Entry;

public class SalaChat implements Entry {

	public Long idSala;
	public String nomeSala;
	public Long idProximaMensagem;

	public SalaChat() {
	}

	public SalaChat(Long idSala, String nomeSala, Long idProximaMensagem) {
		super();
		this.idSala = idSala;
		this.nomeSala = nomeSala;
		this.idProximaMensagem = idProximaMensagem;
	}

	public void incrementar() {
		idProximaMensagem = new Long(idProximaMensagem.longValue() + 1);
	}
}
