package br.com.model;

import net.jini.core.entry.Entry;

public class MensagemSalaChat implements Entry {
	public String origem;
	public String sala;
	public String mensagem;

	public MensagemSalaChat() {
	}

	public MensagemSalaChat(String origem, String sala, String mensagem) {

		this.origem = origem;
		this.sala = sala;
		this.mensagem = mensagem;
	}

}
