package br.com.model;

import net.jini.core.entry.Entry;

public class UsuarioChatSala implements Entry {
	private static final long serialVersionUID = 1L;

	public Long id;
	public String nome;
	public String sala;

	public UsuarioChatSala(String nome, String sala) {
		this.nome = nome;
		this.sala = sala;
	}

	public UsuarioChatSala() {
	}

	public UsuarioChatSala(String nome) {
		this.nome = nome;
	}
}
