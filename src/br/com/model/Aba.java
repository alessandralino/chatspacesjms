package br.com.model;

import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Aba {
	String nomeAba;
	JPanel jPanelAba;
	JTextArea jTextAreaChat;
	SalaChat salaChat;
	UsuarioChat usuarioChat;

	public Aba(String nomeAba, JPanel jPanelAba, JTextArea jTextAreaChat) {
		super();
		this.nomeAba = nomeAba;
		this.jPanelAba = jPanelAba;
		this.jTextAreaChat = jTextAreaChat;
	}


	public String getNomeAba() {
		return nomeAba;
	}

	public void setNomeAba(String nomeAba) {
		this.nomeAba = nomeAba;
	}

	public JPanel getjPanelAba() {
		return jPanelAba;
	}

	public void setjPanelAba(JPanel jPanelAba) {
		this.jPanelAba = jPanelAba;
	}

	public JTextArea getjTextAreaChat() {
		return jTextAreaChat;
	}

	public void setjTextAreaChat(JTextArea jTextAreaChat) {
		this.jTextAreaChat = jTextAreaChat;
	}

	public SalaChat getSalaChat() {
		return salaChat;
	}

	public void setSalaChat(SalaChat salaChat) {
		this.salaChat = salaChat;
	}


	public UsuarioChat getUsuarioChat() {
		return usuarioChat;
	}


	public void setUsuarioChat(UsuarioChat usuarioChat) {
		this.usuarioChat = usuarioChat;
	}
	
	
}
