package br.com.model;

import net.jini.core.entry.Entry;

public class MensagemUsuario implements Entry {
	public String mensagem;
	public String origem;
	public String destino;
	

	public MensagemUsuario() {
	}

	public MensagemUsuario(String mensagem, String origem, String destino) {
		this.mensagem = mensagem;
		this.origem = origem;
		this.destino = destino;
	}
}