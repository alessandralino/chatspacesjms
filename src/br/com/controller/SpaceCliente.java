package br.com.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import br.com.model.MensagemSalaChat;
import br.com.model.MensagemUsuario;
import br.com.model.SalaChat;
import br.com.model.UsuarioChat;
import br.com.model.UsuarioChatSala;
import br.com.view.TelaPrincipal;
import net.jini.core.lease.Lease;
import net.jini.core.transaction.TransactionException;
import net.jini.space.JavaSpace;

public class SpaceCliente {
	private JavaSpace spaceCliente;
	private String nome;
	private String sala;
	private Long idProximaMensagem;
	List<String> listaDeUsuarios;
	List<String> listaDeSalas;

	public SpaceCliente() {
		try {
			Lookup finder = new Lookup(JavaSpace.class);
			spaceCliente = (JavaSpace) finder.getService();
			listaDeUsuarios = new ArrayList<String>();
			listaDeSalas = new ArrayList<String>();
			if (spaceCliente == null) {
				System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
				System.exit(-1);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, " A conexão foi perdida! App será encerrada! :(", "Aviso",
					JOptionPane.WARNING_MESSAGE);
		}

	}

	public boolean entrarNaSala(String nomeUsuario, String sala) {
		try {
			UsuarioChatSala usrSala = new UsuarioChatSala();
			usrSala.nome = nomeUsuario;
			usrSala.sala = sala;
			spaceCliente.write(usrSala, null, Lease.FOREVER);

			return true;
		} catch (Exception ex) {
			TelaPrincipal.encerrarAplicacao();
			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}

	}

	public void escreverMensagemPrivada(String origem, String destino, String mensagem) {
		try {
			MensagemUsuario msg = new MensagemUsuario();
			msg.origem = origem;
			msg.destino = destino;
			msg.mensagem = mensagem;
			spaceCliente.write(msg, null, 5 * 60 * 1000);

		} catch (Exception ex) {
			TelaPrincipal.encerrarAplicacao();
			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void escreverMensagemSala(String origem, String sala, String mensagem) {
		MensagemSalaChat msg = new MensagemSalaChat();
		msg.origem = origem;
		msg.sala = sala;
		msg.mensagem = mensagem;
		try {
			spaceCliente.write(msg, null, 5 * 60 * 1000);
		} catch (RemoteException | TransactionException e) {
			TelaPrincipal.encerrarAplicacao();
			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, e);
		}

	}

	public boolean verificaSeUsarioExiste(String nome) {
		UsuarioChat template = new UsuarioChat();
		template.nome = nome.toLowerCase();
		try {
			UsuarioChat usuarioChat = (UsuarioChat) spaceCliente.read(template, null, 1);
			if (usuarioChat != null) {
				return true;
			} else {
				return false;
			}

		} catch (Exception ex) {
			TelaPrincipal.encerrarAplicacao();

			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
		}

		return false;

	}

	public boolean adicionarUsuario(String nome) {
		if (!verificaSeUsarioExiste(nome)) {
			UsuarioChat usr = new UsuarioChat();
			usr.nome = nome;
			try {
				spaceCliente.write(usr, null, Lease.FOREVER);
				if (!listaDeUsuarios.contains(usr.nome)) {
					listaDeUsuarios.add(usr.nome);
				}

			} catch (Exception ex) {
				TelaPrincipal.encerrarAplicacao();
				Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
			}
			return true;
		} else {
			return false;
		}
	}

	public List<String> findAllUsuarios() {
		List<String> listaUsuariosFound = new ArrayList<String>();
		try {
			UsuarioChat template = new UsuarioChat();
			List<UsuarioChat> listaDeUsuarios = new ArrayList<UsuarioChat>();
			UsuarioChat msg = null;
			do {
				msg = (UsuarioChat) spaceCliente.take(template, null, 1);
				if (msg != null) {
					listaDeUsuarios.add(msg);
					listaUsuariosFound.add(msg.nome.toString());
				}

			} while (msg != null);

			for (UsuarioChat usuarioChat : listaDeUsuarios) {
				if (usuarioChat != null) {
					adicionarUsuario(usuarioChat.nome);
				}

			}

		} catch (Exception ex) {
			TelaPrincipal.encerrarAplicacao();
			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
		}

		return listaUsuariosFound;

	}

	public List<String> findAllUsuariosSala(String sala) {
		List<String> listaUsuariosSalaFound = new ArrayList<String>();
		try {

			for (String usuario : listaDeUsuarios) {
				UsuarioChatSala templateUsuarioChatSala = new UsuarioChatSala();
				templateUsuarioChatSala.nome = usuario;
				templateUsuarioChatSala.sala = sala;
				UsuarioChatSala usuarioChatSala = (UsuarioChatSala) spaceCliente.take(templateUsuarioChatSala, null, 1);

				if (usuarioChatSala != null) {
					listaUsuariosSalaFound.add(usuarioChatSala.nome);
				}
			}

			for (String usuario : listaUsuariosSalaFound) {
				entrarNaSala(usuario, sala);
			}

		} catch (Exception ex) {
			TelaPrincipal.encerrarAplicacao();
			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
		}

		return listaUsuariosSalaFound;
	}

	public List<String> findAllSala() {
		List<String> listaSalaFound = new ArrayList<String>();
		try {
			SalaChat template = new SalaChat();
			List<SalaChat> listaDeSala = new ArrayList<SalaChat>();
			SalaChat msg = null;
			do {
				msg = (SalaChat) spaceCliente.take(template, null, 1);
				if (msg != null) {
					listaDeSala.add(msg);
					listaSalaFound.add(msg.nomeSala.toString());
				}

			} while (msg != null);

			for (SalaChat sala : listaDeSala) {
				if (sala != null) {
					criarSala(sala.nomeSala);
				}

			}

		} catch (Exception ex) {
			TelaPrincipal.encerrarAplicacao();
			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
		}

		return listaSalaFound;
	}

	public boolean verificaSeSalaExiste(String nome) {
		SalaChat template = new SalaChat();
		template.nomeSala = nome.toLowerCase();
		try {
			SalaChat salaChat = (SalaChat) spaceCliente.read(template, null, 1);
			if (salaChat != null) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			TelaPrincipal.encerrarAplicacao();
			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public boolean criarSala(String nome) {
		if (!verificaSeUsarioExiste(nome)) {
			SalaChat salaChat = new SalaChat();
			salaChat.nomeSala = nome.toLowerCase();
			try {
				SalaChat sala = new SalaChat(proxNumeroSalaDisponivel(), nome, new Long(0));
				spaceCliente.write(sala, null, 10 * 60 * 1000);
				if (!listaDeSalas.contains(salaChat.nomeSala)) {
					listaDeSalas.add(salaChat.nomeSala);
				}

			} catch (Exception ex) {
				TelaPrincipal.encerrarAplicacao();
				Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
			}
			return true;
		} else {
			return false;
		}
	}

	public Long proxNumeroSalaDisponivel() {
		Long quantidadeDeSala = new Long(0);
		quantidadeDeSala = (long) listaDeSalas.size();
		return quantidadeDeSala;
	}

	public List<String> findAllMensagensSala(String sala) {
		List<String> listaMensagensFound = new ArrayList<String>();
		try {
			MensagemSalaChat template = new MensagemSalaChat();
			template.sala = sala;
			List<MensagemSalaChat> listaDeMensagensSalaChat = new ArrayList<MensagemSalaChat>();
			MensagemSalaChat msg = null;
			do {
				msg = (MensagemSalaChat) spaceCliente.take(template, null, 1);
				if (msg != null) {
					listaDeMensagensSalaChat.add(msg);
					listaMensagensFound.add(msg.origem + " disse: " + msg.mensagem);
					
				}

			} while (msg != null);

			for (MensagemSalaChat mensagemSala : listaDeMensagensSalaChat) {
				if (mensagemSala != null) {
					escreverMensagemPrivada(mensagemSala.origem, mensagemSala.sala, mensagemSala.mensagem);
				}
			}

		} catch (Exception ex) {
			TelaPrincipal.encerrarAplicacao();
			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
		}

		return listaMensagensFound;
	}

	public List<String> findAllMensagensUsuario(String destino) {
		List<String> listaMensagensFound = new ArrayList<String>();
		try {
			MensagemUsuario template = new MensagemUsuario();
			template.destino = destino;
			List<MensagemUsuario> listaDeMensagensPrivadas = new ArrayList<MensagemUsuario>();
			MensagemUsuario msg = null;
			do {
				msg = (MensagemUsuario) spaceCliente.take(template, null, 1);
				if (msg != null) {
					listaDeMensagensPrivadas.add(msg);
					listaMensagensFound.add(msg.origem + " disse: " + msg.mensagem);
				}

			} while (msg != null);

			for (MensagemUsuario mensagemUsuario : listaDeMensagensPrivadas) {
				if (mensagemUsuario != null) {
					escreverMensagemPrivada(mensagemUsuario.origem, mensagemUsuario.destino, mensagemUsuario.mensagem);
				}
			}

		} catch (Exception ex) {
			TelaPrincipal.encerrarAplicacao();
			Logger.getLogger(SpaceCliente.class.getName()).log(Level.SEVERE, null, ex);
		}

		return listaMensagensFound;
	}

	public JavaSpace getSpaceCliente() {
		return spaceCliente;
	}

	public void setSpaceCliente(JavaSpace spaceCliente) {
		this.spaceCliente = spaceCliente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSala() {
		return sala;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}

	public Long getIdProximaMensagem() {
		return idProximaMensagem;
	}

	public void setIdProximaMensagem(Long idProximaMensagem) {
		this.idProximaMensagem = idProximaMensagem;
	}
}
