package br.com.view;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.com.controller.SpaceCliente;

public class Registro {

	private SpaceCliente cliente;
	private boolean isConectada = true;

	public static void main(String[] args) {
		Registro r = new Registro();
		r.inicializaRegistro();
	}

	public void inicializaRegistro() {
		String[] options = { "OK" };
		JPanel panel = new JPanel();
		JLabel lbl = new JLabel("Nome do Cliente: ");
		JTextField jTextFieldNomeCliente = new JTextField(10);
		panel.add(lbl);
		panel.add(jTextFieldNomeCliente);
		int selectedOption = JOptionPane.showOptionDialog(null, panel, "Registrar cliente", JOptionPane.NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

		if (selectedOption == 0) {
			String nome = jTextFieldNomeCliente.getText().trim();

			if (nome.trim().equals("")) {
				JOptionPane.showMessageDialog(null, "Informe um nome não vazio!", "Aviso", JOptionPane.WARNING_MESSAGE);
				return;
			}

			isConectada = true;
			new Thread(new Runnable() {
				public void run() {
					while (isConectada) {
						cliente = new SpaceCliente();
						isConectada = false;
					}
				}
			}).start();

			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
				Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
			}

			while (isConectada) {
				Object[] erroRegistro = { "Tentar Novamente!", "Encerrar!" };
				int statusConexao = JOptionPane.showOptionDialog(null, "Não foi possível conectar ao servidor ", "Aviso",
						JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, erroRegistro, erroRegistro[0]);

				if (statusConexao == 1) {
					System.exit(0);
				}
			}

			if (cliente.adicionarUsuario(nome)) {
				cliente.setNome(nome);
				
				new TelaPrincipal(cliente);
				
			} else {
				JOptionPane.showMessageDialog(null, "Usuário Já Logado! ", "Aviso", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
}
