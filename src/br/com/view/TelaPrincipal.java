package br.com.view;

import java.awt.Color;

import java.awt.Dimension;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import br.com.controller.SpaceCliente;
import br.com.model.Aba;
import br.com.model.SalaChat;
import br.com.model.UsuarioChat;

public class TelaPrincipal {
	JFrame jFrameTelaPrincipal;

	JPanel painelAbasMensagem;
	JPanel painelAbas;
	JPanel painelMensagem;
	JPanel painelListaDeSalasAcoesDoUsuario;
	JPanel painelListaDeSalas;
	JPanel painelAcoesDoUsuario;

	JTabbedPane jTabbedPaneSalaChat;

	JTextField JTextFieldNomeNovaSala;
	JTextField jTextFieldMensagem;

	JButton jButtonEnviarMensagem;
	JButton jButtonCriarNovaSala;

	List<Aba> listaDeAbas;
	List<String> listaDeChats;
	List<String> listaDeSalas;
	List<DefaultMutableTreeNode> listaDeNoSalas = new ArrayList<DefaultMutableTreeNode>();

	DefaultMutableTreeNode root;
	JTree jTree;
	JTree tree;
	JMenuItem jMenuItemAdicionarUsuarioSala;

	List<String> listaMensagensPrivadas;
	List<String> listaMensagensSalaChat;
	List<String> bufferListaMensagens;
	List<String> bufferListaMensagensSalaChat;
	List<String> bufferListaSalas;
	List<String> bufferListaUsuariosSala;
	SpaceCliente cliente;

	public TelaPrincipal(final SpaceCliente cliente) {
		super();

		this.cliente = cliente;
		this.jMenuItemAdicionarUsuarioSala = new JMenuItem("Adicionar usuário a uma sala");
		inicializaJTree();
		inicializaTelaPrincipal();
		inicializaPainelAbasMensagem();
		inicializaListaDeSalasAcoesDoUsuario();

		listaDeAbas = new ArrayList<Aba>();
		listaDeChats = new ArrayList<String>();
		listaDeSalas = new ArrayList<String>();

		bufferListaMensagens = new ArrayList<String>();
		bufferListaMensagensSalaChat = new ArrayList<String>();
		bufferListaSalas = new ArrayList<String>();
		bufferListaUsuariosSala = new ArrayList<String>();

		listaDeChats = cliente.findAllUsuarios();
		listaDeSalas = cliente.findAllSala();

		jTabbedPaneSalaChat = new JTabbedPane();

		jFrameTelaPrincipal.add(painelAbasMensagem);
		jFrameTelaPrincipal.add(painelListaDeSalasAcoesDoUsuario);

		JPanel jp = new JPanel();

		Aba novaAba = new Aba(new String(), jp, new JTextArea());
		listaDeAbas.add(novaAba);
		jTabbedPaneSalaChat.addTab("", jp);

		painelAbas.add(jTabbedPaneSalaChat);

		new Thread(new Runnable() {
			public void run() {
				while (true) {
					List<String> listaAtualUsuarios = cliente.findAllUsuarios();
					listaMensagensPrivadas = cliente.findAllMensagensUsuario(cliente.getNome());

					if (listaDeAbas.size() > 1) {
						for (int i = 1; i < listaDeAbas.size(); i++) {
							Aba aba = listaDeAbas.get(i);
							if (!aba.getNomeAba().equals(cliente.getNome())) {
								for (String mensagem : listaMensagensPrivadas) {
									if (mensagem.contains(aba.getNomeAba())
											&& !bufferListaMensagens.contains(mensagem)) {
										aba.getjTextAreaChat().append(mensagem + "\r\n");
										bufferListaMensagens.add(mensagem);
									}
								}

							}

						}
					}
					
					if (listaDeAbas.size() > 1) {
						for (int i = 1; i < listaDeAbas.size(); i++) {
							Aba aba = listaDeAbas.get(i);

							listaMensagensSalaChat = cliente.findAllMensagensSala(aba.getNomeAba());

							for (String mensagemSala : listaMensagensSalaChat) {
								System.out.println(aba.getNomeAba());
								// if
								// (!bufferListaMensagensSalaChat.contains(mensagemSala))
								// {
								aba.getjTextAreaChat().append(mensagemSala + "\r\n");
								bufferListaMensagensSalaChat.add(mensagemSala);
								System.out.println(mensagemSala);
								// }
							}
						}
					}
					

					for (String novoUsuario : listaAtualUsuarios) {
						if (!listaDeChats.contains(novoUsuario)) {
							if (!novoUsuario.equalsIgnoreCase(cliente.getNome())) {
								criaNovoNo(novoUsuario);
								atualizaArvoreDeConversas();
								listaDeChats.add(novoUsuario);
							}

						}

					}

					List<String> listaAtualSalas = cliente.findAllSala();

					for (String novaSala : listaAtualSalas) {
						if (!listaDeSalas.contains(novaSala) && !bufferListaSalas.contains(novaSala)) {
							criaNovoNo(novaSala);
							// listaDeNoSalas.add(new
							// DefaultMutableTreeNode(novaSala));
							atualizaArvoreDeConversas();
							listaDeSalas.add(novaSala);
							bufferListaSalas.add(novaSala);

						}

					}

					int index = 0;
					for (String sala : listaAtualSalas) {
						List<String> usuariosSala = cliente.findAllUsuariosSala(sala);

						for (String novoSubNo : usuariosSala) {
							if (!bufferListaUsuariosSala.contains(novoSubNo)) {
								criaSubNo(root, listaDeNoSalas.get(index), novoSubNo);
								atualizaArvoreDeConversas();
								bufferListaUsuariosSala.add(novoSubNo);
							}

						}
						index++;
					}

					try {
						Thread.sleep(3000);
					} catch (InterruptedException ex) {
						Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
					}
				}

			}

		}).start();

		jButtonCriarNovaSala.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JTextFieldNomeNovaSala = new JTextField();

				String[] options = { "OK" };
				JPanel panel = new JPanel();
				JLabel lbl = new JLabel("Nome da sala: ");
				final JTextField jTextFieldNomeSalaNova = new JTextField(10);
				panel.add(lbl);
				panel.add(jTextFieldNomeSalaNova);
				int selectedOption = JOptionPane.showOptionDialog(null, panel, "Criar nova sala", JOptionPane.NO_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

				if (selectedOption == 0) {
					String nomeSalaNova = jTextFieldNomeSalaNova.getText();
					boolean isNomeSalaJaExiste = false;

					for (String sala : listaDeSalas) {
						if (sala.equalsIgnoreCase(nomeSalaNova) && isNomeSalaJaExiste == false) {
							isNomeSalaJaExiste = true;
						}
					}

					if (isNomeSalaJaExiste == false) {
						JPanel jPanelNovaSala = new JPanel();
						jPanelNovaSala.setName(nomeSalaNova);

						JTextArea jTextAreaNovaSala = new JTextArea();
						jTextAreaNovaSala.setPreferredSize(new Dimension(400, 400));
						jTextAreaNovaSala.setBackground(Color.WHITE);
						jTextAreaNovaSala.setEditable(false);
						jPanelNovaSala.add(jTextAreaNovaSala);

						Aba novaAba = new Aba(nomeSalaNova, jPanelNovaSala, jTextAreaNovaSala);

						SalaChat novaSala = new SalaChat();
						novaSala.nomeSala = nomeSalaNova;
						novaAba.setSalaChat(novaSala);
						novaAba.setUsuarioChat(null);

						listaDeAbas.add(novaAba);
						listaDeNoSalas.add(new DefaultMutableTreeNode(nomeSalaNova));
						painelAbas.add(jTabbedPaneSalaChat);

						cliente.criarSala(nomeSalaNova);

					} else {
						JOptionPane.showMessageDialog(null, "Esse nome de sala já existe!");
					}

				}
			}
		});

		jTabbedPaneSalaChat.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					if (jTabbedPaneSalaChat.getTabCount() > 0) {
						int index = jTabbedPaneSalaChat.getSelectedIndex();
						if (index == 0) {
							return;
						}

						if (index != -1) {
							String us = jTabbedPaneSalaChat.getTitleAt(index);
							if (JOptionPane.showConfirmDialog(null,
									"Você deseja sair da conversa com " + jTabbedPaneSalaChat.getTitleAt(index) + "?",
									"Aviso!", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
								jTabbedPaneSalaChat.remove(index);

								int i = 0;
								for (Aba aba : listaDeAbas) {
									if (aba.getNomeAba().equals(us)) {
										listaDeAbas.remove(i);
										return;
									}
									i++;
								}

							}
						}
					}
				}
			}
		});

		jButtonEnviarMensagem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!jTextFieldMensagem.getText().trim().equals("")) {
					int index = jTabbedPaneSalaChat.getSelectedIndex();
					listaDeAbas.get(index).getjTextAreaChat()
							.append("Você disse: " + jTextFieldMensagem.getText() + "\r\n");
					Aba abaAtual = listaDeAbas.get(index);
					if (abaAtual.getSalaChat() == null) {
						String origem = cliente.getNome();
						String destino = listaDeAbas.get(index).getNomeAba();
						String mensagem = jTextFieldMensagem.getText();
						cliente.escreverMensagemPrivada(origem, destino, mensagem);
					} else {
						System.out.println("é uma sala chat");
					}

				}
				jTextFieldMensagem.setText("");
			}
		});

		jTextFieldMensagem.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (!jTextFieldMensagem.getText().trim().equals("")) {
						int index = jTabbedPaneSalaChat.getSelectedIndex();
						listaDeAbas.get(index).getjTextAreaChat()
								.append("Você disse: " + jTextFieldMensagem.getText() + "\r\n");
						Aba abaAtual = listaDeAbas.get(index);
						if (abaAtual.getSalaChat() == null) {
							String origem = cliente.getNome();
							String destino = listaDeAbas.get(index).getNomeAba();
							String mensagem = jTextFieldMensagem.getText();
							cliente.escreverMensagemPrivada(origem, destino, mensagem);
						} else {
							String origem = cliente.getNome();
							String sala = listaDeAbas.get(index).getNomeAba();
							String mensagem = jTextFieldMensagem.getText();
							cliente.escreverMensagemSala(origem, sala, mensagem);
						}

					}
					jTextFieldMensagem.setText("");
				}

			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		MouseAdapter ma = new MouseAdapter() {
			private void myPopupEvent(MouseEvent e) {
				int x = e.getX();
				int y = e.getY();
				JTree tree = (JTree) e.getSource();
				TreePath path = tree.getPathForLocation(x, y);
				if (path == null)
					return;

				tree.setSelectionPath(path);

				JPopupMenu popup = new JPopupMenu();

				popup.add(jMenuItemAdicionarUsuarioSala);
				popup.show(tree, x, y);

			}

			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger())
					myPopupEvent(e);
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger())
					myPopupEvent(e);
			}
		};

		tree.addMouseListener(ma);
		tree.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
					if (node == null)
						return;

					DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
					if (selectedNode != null) {
						String nomeSalaConversaNova = selectedNode.getUserObject().toString();
						boolean criarNovaAba = true;

						for (Aba aba : listaDeAbas) {
							if (aba.getNomeAba().equals(nomeSalaConversaNova)) {
								criarNovaAba = false;
							}
						}

						if (criarNovaAba) {
							JPanel jPanelNovaConversa = new JPanel();
							jPanelNovaConversa.setName(nomeSalaConversaNova);

							JTextArea jTextAreaNovaSala = new JTextArea();
							jTextAreaNovaSala.setPreferredSize(new Dimension(400, 400));
							jTextAreaNovaSala.setBackground(Color.WHITE);
							jTextAreaNovaSala.setEditable(false);
							jPanelNovaConversa.add(jTextAreaNovaSala);

							boolean isChat = false;

							for (String chat : listaDeChats) {
								if (chat.equals(nomeSalaConversaNova)) {
									isChat = true;
								}
							}

							if (!isChat) {
								Aba novaAba = new Aba(nomeSalaConversaNova, jPanelNovaConversa, jTextAreaNovaSala);
								SalaChat novaSala = new SalaChat();
								novaSala.nomeSala = nomeSalaConversaNova;
								novaAba.setSalaChat(novaSala);
								listaDeAbas.add(novaAba);
								jTabbedPaneSalaChat.addTab(nomeSalaConversaNova, jPanelNovaConversa);
								painelAbas.add(jTabbedPaneSalaChat);
							} else {
								Aba novaAba = new Aba(nomeSalaConversaNova, jPanelNovaConversa, jTextAreaNovaSala);
								UsuarioChat novoUsuarioChat = new UsuarioChat();
								novoUsuarioChat.nome = nomeSalaConversaNova;
								novaAba.setUsuarioChat(novoUsuarioChat);
								listaDeAbas.add(novaAba);
								jTabbedPaneSalaChat.addTab(nomeSalaConversaNova, jPanelNovaConversa);
								painelAbas.add(jTabbedPaneSalaChat);
							}

						}
					}
				}
			}
		});

		jMenuItemAdicionarUsuarioSala.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<String> listaDeSalas = cliente.findAllSala();

				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
				String nomeUsuarioNovoNaSala = selectedNode.getUserObject().toString();

				for (String ls : listaDeSalas) {
					List<String> listaDeUsuarioSala = cliente.findAllUsuariosSala(ls);

					if (listaDeUsuarioSala.contains(nomeUsuarioNovoNaSala)) {
						JOptionPane.showMessageDialog(null,
								"O usuário " + nomeUsuarioNovoNaSala + " já está em outra sala!", "Aviso",
								JOptionPane.WARNING_MESSAGE);
						return;
					}
				}

				if (!listaDeSalas.contains(nomeUsuarioNovoNaSala)) {
					int index = selectedNode.getChildCount();

					int tamanho = listaDeSalas.size();
					String[] values = new String[tamanho];

					for (int i = 0; i < listaDeSalas.size(); i++) {
						values[i] = listaDeSalas.get(i);
					}

					Object salaEscolhida = JOptionPane.showInputDialog(null, "Salas disponíveis:", "Selecione uma sala",
							JOptionPane.DEFAULT_OPTION, null, values, "Selecione uma sala");
					if (salaEscolhida != null) {
						String sala = salaEscolhida.toString();

						cliente.entrarNaSala(nomeUsuarioNovoNaSala, sala);
						List<String> lis = cliente.findAllUsuariosSala(sala);

						int i = 0;
						listaDeSalas = cliente.findAllSala();

						if (listaDeSalas != null) {
							for (String ls : listaDeSalas) {
								if (ls.equals(sala)) {
									index = i;
								}
								i++;
							}

							for (String usuario : lis) {
								if (nomeUsuarioNovoNaSala.equals(usuario)
										&& bufferListaUsuariosSala.contains(usuario)) {
									criaSubNo(root, listaDeNoSalas.get(index), usuario);
									DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
									root = (DefaultMutableTreeNode) model.getRoot();
									model.reload(root);

								}

							}

						}

					}
				} else {
					JOptionPane.showMessageDialog(null, "Não se pode adicionar uma sala a outra!", "Aviso",
							JOptionPane.WARNING_MESSAGE);
				}

			}
		});

		JScrollPane jScrollPaneListaDeSalasEusuarios = new JScrollPane(tree);
		jScrollPaneListaDeSalasEusuarios.setPreferredSize(new Dimension(400, 455));

		painelListaDeSalas.add(jScrollPaneListaDeSalasEusuarios);
		jFrameTelaPrincipal.setVisible(true);

	}

	public void atualizaArvoreDeConversas() {
		DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
		root = (DefaultMutableTreeNode) model.getRoot();
		model.reload(root);
	}

	public void inicializaJTree(DefaultMutableTreeNode root) {
		jTree = new JTree(root);
	}

	public DefaultMutableTreeNode criaNoSala(final String nomeSalaNova) {
		DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
		root = (DefaultMutableTreeNode) model.getRoot();
		DefaultMutableTreeNode novoNoSala = new DefaultMutableTreeNode(nomeSalaNova);
		root.add(novoNoSala);
		model.reload(root);
		return novoNoSala;
	}

	public void inicializaListaDeSalasAcoesDoUsuario() {
		painelAcoesDoUsuario = new JPanel();
		painelListaDeSalasAcoesDoUsuario = new JPanel();
		painelListaDeSalas = new JPanel();

		jButtonCriarNovaSala = new JButton("Criar nova sala");

		painelAcoesDoUsuario.setBackground(Color.WHITE);

		painelListaDeSalas.setPreferredSize(new Dimension(400, 460));
		painelAcoesDoUsuario.setPreferredSize(new Dimension(400, 40));
		jButtonCriarNovaSala.setPreferredSize(new Dimension(150, 30));

		painelAcoesDoUsuario.add(jButtonCriarNovaSala);

		inicializaJTree();
		List<String> listaUsuariosFound = cliente.findAllUsuarios();
		List<String> listaSalaFound = cliente.findAllSala();

		for (String usuarios : listaUsuariosFound) {
			if (!usuarios.equalsIgnoreCase(cliente.getNome())) {
				criaNovoNo(usuarios);
			}

		}

		for (String sala : listaSalaFound) {
			DefaultMutableTreeNode novoNoSala = criaNovoNo(sala);
			listaDeNoSalas.add(novoNoSala);
		}

		tree = new JTree(root);

		tree.setShowsRootHandles(true);
		tree.setRootVisible(false);

		painelListaDeSalas.add(tree);

		painelListaDeSalas.setBackground(Color.white);
		painelListaDeSalasAcoesDoUsuario.add(painelListaDeSalas);
		painelListaDeSalasAcoesDoUsuario.add(painelAcoesDoUsuario);
		painelListaDeSalasAcoesDoUsuario.setBackground(Color.WHITE);

	}

	public void inicializaTelaPrincipal() {
		jFrameTelaPrincipal = new JFrame("spaceChat :. Bem-vind@, " + cliente.getNome());
		GridLayout layoutTelaPrincipal = new GridLayout(1, 2);
		jFrameTelaPrincipal.setLayout(layoutTelaPrincipal);
		jFrameTelaPrincipal.setSize(900, 550);
		jFrameTelaPrincipal.setResizable(false);
		jFrameTelaPrincipal.setLocationRelativeTo(null);
		jFrameTelaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrameTelaPrincipal.setBackground(Color.WHITE);

	}

	public void inicializaJTree() {
		jTree = new JTree();
		root = new DefaultMutableTreeNode("Root");
	}

	public DefaultMutableTreeNode criaNovoNo(String nomeNovoNo) {
		DefaultMutableTreeNode novoNo = new DefaultMutableTreeNode(nomeNovoNo);
		root.add(novoNo);
		return novoNo;
	}

	public void criaSubNo(DefaultMutableTreeNode root, DefaultMutableTreeNode novoNo, String novoSubNo) {
		novoNo.add(new DefaultMutableTreeNode(novoSubNo));

	}

	public void inicializaPainelAbasMensagem() {
		painelAbasMensagem = new JPanel();
		painelMensagem = new JPanel();
		painelAbas = new JPanel();

		GridLayout layoutAbas = new GridLayout(1, 1);
		painelAbas.setLayout(layoutAbas);
		painelMensagem.setBackground(Color.WHITE);

		painelAbas.setPreferredSize(new Dimension(400, 450));
		painelMensagem.setPreferredSize(new Dimension(400, 50));

		jTextFieldMensagem = new JTextField();
		jTextFieldMensagem.setPreferredSize(new Dimension(300, 30));
		painelMensagem.add(jTextFieldMensagem);

		jButtonEnviarMensagem = new JButton("Enviar");
		jButtonEnviarMensagem.setPreferredSize(new Dimension(80, 30));
		painelMensagem.add(jButtonEnviarMensagem);

		painelAbasMensagem.add(painelAbas);
		painelAbasMensagem.add(painelMensagem);
		painelAbasMensagem.setBackground(Color.WHITE);
		painelAbas.setBackground(Color.LIGHT_GRAY);
	}

	public static void encerrarAplicacao() {
		JOptionPane.showMessageDialog(null, " A conexão foi perdida. A aplicação será encerrada!", "Aviso",
				JOptionPane.WARNING_MESSAGE);
		System.exit(-1);
	}

	public DefaultMutableTreeNode getRoot() {
		return root;
	}

	public void setRoot(DefaultMutableTreeNode root) {
		this.root = root;
	}

	public SpaceCliente getCliente() {
		return cliente;
	}

	public void setCliente(SpaceCliente cliente) {
		this.cliente = cliente;
	}

	public JTree getjTree() {
		return jTree;
	}

	public void setjTree(JTree jTree) {
		this.jTree = jTree;
	}

}
